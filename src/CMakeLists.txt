add_definitions(
    -Dunix
    -D_BSD
    -D_ALL_SOURCE
    -D_LARGEFILE_SOURCE
    -D_FILE_OFFSET_BITS=64
    -DNL_THREADSAFE
    -D_REENTRANT
    -DOO_STD
    -Wno-deprecated
)

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    add_definitions(
        -DLINUX
        -D_GNU_SOURCE
    )
elseif(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    add_definitions(
        -DMACOS
    )
endif()

add_executable(${PROJECT_NAME}
        bbcp_Args.cpp                 
        bbcp_BuffPool.cpp
        bbcp_C32.cpp
        bbcp_ChkSum.cpp
        bbcp_Config.cpp
        bbcp.cpp
        bbcp_Emsg.cpp
        bbcp_File.cpp
        bbcp_FileSpec.cpp
        bbcp_FileSystem.cpp
        bbcp_FS_Null.cpp
        bbcp_FS_Pipe.cpp
        bbcp_FS_Unix.cpp
        bbcp_IO.cpp
        bbcp_IO_Null.cpp
        bbcp_IO_Pipe.cpp
        bbcp_Link.cpp
        bbcp_LogFile.cpp
        bbcp_MD5.cpp
        bbcp_MD5_openssl.cpp
        bbcp_NetAddr.cpp
        bbcp_NetAddrInfo.cpp
        bbcp_NetLogger.cpp
        bbcp_Network.cpp
        bbcp_Node.cpp
        bbcp_ProcMon.cpp
        bbcp_ProgMon.cpp
        bbcp_Protocol.cpp
        bbcp_Pthread.cpp
        bbcp_RTCopy.cpp
        bbcp_Set.cpp
        bbcp_Stream.cpp
        bbcp_System.cpp
        bbcp_Timer.cpp
        bbcp_Version.cpp
        bbcp_ZCX.cpp
)

find_package(Threads REQUIRED)
pkg_check_modules(CRYPTO libcrypto REQUIRED)
pkg_check_modules(ZLIB zlib REQUIRED)

link_directories(
        ${CRYPTO_LIBRARY_DIRS}
        ${ZLIB_LIBRARY_DIRS}
)

include_directories(
        ${CRYPTO_INCLUDE_DIRS}
        ${ZLIB_INCLUDE_DIRS}
)

target_link_libraries(${PROJECT_NAME}
        Threads::Threads
        ${ZLIB_LIBRARIES}
        ${CRYPTO_LIBRARIES}
)
